package raghu.test.drawarc

import android.content.Context
import android.graphics.BitmapFactory
import android.graphics.Color
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import android.widget.FrameLayout
import android.widget.ImageView

/**
 * Created by klogi on 1/11/2016.
 */
class BarometerView : FrameLayout {
    var barometer = BarometerDrawable(context, Color.parseColor("#BDC4CC"), Color.parseColor("#EFEFEF"), Color.parseColor("#656764"))

    constructor(context: Context) : super(context) {
        initializeView(context)
    }

    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs) {
        initializeView(context)
    }

    private fun initializeView(context: Context) {
        LayoutInflater.from(context).inflate(R.layout.barometer_view, this)
        background = barometer
    }

    fun setImage(drawableRes: Int) {
        val circleTransform = CircleTransform()
        val image = BitmapFactory.decodeResource(context.resources, drawableRes)
        val transformedImage = circleTransform.transform(image)
        (rootView.findViewById<View>(R.id.userPhotoImageView) as ImageView).setImageBitmap(transformedImage)
    }

    fun setValue(value: Double) {
        barometer.setValue(value)
    }
}