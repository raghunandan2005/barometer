package raghu.test.drawarc

import android.content.Context
import android.graphics.*
import android.graphics.drawable.Drawable


class BarometerDrawable(context: Context, backgroundColor: Int, foregroundColor: Int, strokeColor: Int) : Drawable() {

    private val paintBackground: Paint = Paint()
    private val paintForeground: Paint
    private val paintStroke: Paint
    private val paintRed: Paint
    private val paintYellow: Paint
    private val paintGreen: Paint
    private val paintRedStroke: Paint
    private val paintYellowStroke: Paint
    private val paintGreenStroke: Paint
    private val paintGreenFillingStroke: Paint
    private val paintYellowFillingStroke: Paint
    private val paintRedFillingStroke: Paint
    private val rectFBackground: RectF
    private val rectFForeground: RectF
    private val rectFStroke1: RectF
    private val rectFStroke2: RectF
    private val rectFStrokeColor: RectF
    private val rectFBottomBackground: RectF
    private val rectFSector: RectF
    private val rectFAvatar: RectF
    private val rectFSeparator: RectF
    private var starBitmap: Bitmap
    private var value = 0.0

    override fun draw(canvas: Canvas) {
        canvas.save()
        canvas.scale(1f, 2f)
        val foregroundPadding = bounds.width() / 60
        rectFBackground.set(bounds)
        //set(float left, float top, float right, float bottom)
        rectFForeground[bounds.left + foregroundPadding * 2.toFloat(), bounds.top + foregroundPadding.toFloat(), bounds.right - foregroundPadding * 2.toFloat()] = bounds.bottom - foregroundPadding.toFloat()
        rectFStroke1[bounds.left + foregroundPadding * 6.toFloat(), bounds.top + foregroundPadding * 3.toFloat(), bounds.right - foregroundPadding * 6.toFloat()] = bounds.bottom - foregroundPadding * 3.toFloat()
        rectFStroke2[bounds.left + foregroundPadding * 8.toFloat(), bounds.top + foregroundPadding * 4.toFloat(), bounds.right - foregroundPadding * 8.toFloat()] = bounds.bottom - foregroundPadding * 4.toFloat()
        rectFStrokeColor[bounds.left + foregroundPadding * 7 - 1.toFloat(), bounds.top + foregroundPadding * 3.5f - 1, bounds.right - foregroundPadding * 7 - 1.toFloat()] = bounds.bottom - foregroundPadding * 3.5f - 1
        rectFBottomBackground[bounds.left + foregroundPadding * 2.toFloat(), bounds.height() / 2 - foregroundPadding.toFloat(), bounds.right - foregroundPadding * 2.toFloat()] = bounds.height() / 2.toFloat()
        rectFSector[bounds.width() / 4.toFloat(), bounds.height() / 4 - foregroundPadding.toFloat(), 3 * bounds.width() / 4.toFloat()] = 3 * bounds.height() / 4 - foregroundPadding.toFloat()
        rectFAvatar[bounds.width() / 2 - foregroundPadding * 4.toFloat(), bounds.height() / 2 - 4 * foregroundPadding.toFloat(), bounds.width() / 2 + foregroundPadding * 4.toFloat()] = bounds.height() / 2.toFloat()
        canvas.drawArc(rectFBackground, -180f, 180f, true, paintBackground)
        canvas.drawArc(rectFForeground, -180f, 180f, true, paintForeground)
        canvas.drawArc(rectFStroke1, -180f, 180f, false, paintStroke)
        canvas.drawArc(rectFStroke2, -180f, 180f, false, paintStroke)
        canvas.drawArc(rectFSector, -55f, 55f, true, paintGreen)
        canvas.drawArc(rectFSector, -125f, 70f, true, paintYellow)
        canvas.drawArc(rectFSector, -180f, 55f, true, paintRed)
        canvas.drawArc(rectFStrokeColor, -178f, 26f, false, paintRedStroke)
        canvas.drawArc(rectFStrokeColor, -148f, 26f, false, paintRedStroke)
        canvas.drawArc(rectFStrokeColor, -118f, 26f, false, paintYellowStroke)
        canvas.drawArc(rectFStrokeColor, -88f, 26f, false, paintYellowStroke)
        canvas.drawArc(rectFStrokeColor, -58f, 26f, false, paintGreenStroke)
        canvas.drawArc(rectFStrokeColor, -28f, 26f, false, paintGreenStroke)
        canvas.drawArc(rectFAvatar, -180f, 360f, true, paintForeground)
        val starRatingRectF = RectF()
        val starHeight = bounds.width() / 30.toFloat()
        val starWidth = starHeight * 1.3f
        starBitmap = Bitmap.createScaledBitmap(starBitmap, starWidth.toInt(), starHeight.toInt(), false)

        // first sector
        for (i in 1..1) {
            // cos(.26179) is .9659.
            // 0.9659*0.87 is 0.84035.
            // 1- 0.84035 is 0.15965.
            //  getbounds returns RectF bounds for the drawable.
            // .width() gives the width of the drawable. width/2 gives the center of the drawable.
            val starCenterX = ((1 - Math.cos(0.261799) * 0.87) * (bounds.width() / 2)).toFloat()
            val starCenterY = ((1 - Math.sin(0.261799) * 0.87) * (bounds.height() / 2)).toFloat()
            starRatingRectF[starCenterX - starWidth / 2, starCenterY - starHeight / 2, starCenterX + starWidth / 2] = starCenterY + starHeight / 2
            val matrix = Matrix()
            matrix.postRotate(15f)
            val rotatedStar = Bitmap.createBitmap(starBitmap, 0, 0, starWidth.toInt(), starHeight.toInt(), matrix, false)
            canvas.drawBitmap(rotatedStar, null, starRatingRectF, null)
        }

        // second sector
        for (i in 1..2) {
            val starCenterX = ((1 - Math.cos(0.523599 + 0.174533 * i) * 0.87) * (bounds.width() / 2)).toFloat()
            val starCenterY = ((1 - Math.sin(0.523599 + 0.174533 * i) * 0.87) * (bounds.height() / 2)).toFloat()
            starRatingRectF[starCenterX - starWidth / 2, starCenterY - starHeight / 2, starCenterX + starWidth / 2] = starCenterY + starHeight / 2
            val matrix = Matrix()
            matrix.postRotate(45f)
            val rotatedStar = Bitmap.createBitmap(starBitmap, 0, 0, starWidth.toInt(), starHeight.toInt(), matrix, false)
            canvas.drawBitmap(rotatedStar, null, starRatingRectF, null)
        }

        // third sector (yellow)
        for (i in 1..3) {
            val starCenterX = ((1 - Math.cos(1.0472 + 0.1309 * i) * 0.87) * (bounds.width() / 2)).toFloat()
            val starCenterY = ((1 - Math.sin(1.0472 + 0.1309 * i) * 0.87) * (bounds.height() / 2)).toFloat()
            starRatingRectF[starCenterX - starWidth / 2, starCenterY - starHeight / 2, starCenterX + starWidth / 2] = starCenterY + starHeight / 2
            val matrix = Matrix()
            matrix.postRotate(75f)
            val rotatedStar = Bitmap.createBitmap(starBitmap, 0, 0, starWidth.toInt(), starHeight.toInt(), matrix, false)
            canvas.drawBitmap(rotatedStar, null, starRatingRectF, null)
        }


        // fourth sector (yellow)
        for (i in 1..4) {
            val starCenterX = ((1 - Math.cos(1.5708 + 0.10472 * i) * 0.87) * (bounds.width() / 2)).toFloat()
            val starCenterY = ((1 - Math.sin(1.5708 + 0.10472 * i) * 0.87) * (bounds.height() / 2)).toFloat()
            starRatingRectF[starCenterX - starWidth / 2, starCenterY - starHeight / 2, starCenterX + starWidth / 2] = starCenterY + starHeight / 2
            val matrix = Matrix()
            matrix.postRotate(105f)
            val rotatedStar = Bitmap.createBitmap(starBitmap, 0, 0, starWidth.toInt(), starHeight.toInt(), matrix, false)
            canvas.drawBitmap(rotatedStar, null, starRatingRectF, null)
        }

        // fifth sector (green), bit one
        for (i in 1..5) {
            val starCenterX = ((1 - Math.cos(2.0944 + 0.154533 * i) * 0.87) * (bounds.width() / 2)).toFloat()
            val starCenterY = ((1 - Math.sin(2.0944 + 0.1574533 * i) * 0.87) * (bounds.height() / 2)).toFloat()
            starRatingRectF[starCenterX - starWidth / 2, starCenterY - starHeight / 2, starCenterX + starWidth / 2] = starCenterY + starHeight / 2
            val matrix = Matrix()
            matrix.postRotate(145f)
            val rotatedStar = Bitmap.createBitmap(starBitmap, 0, 0, starWidth.toInt(), starHeight.toInt(), matrix, false)
            canvas.drawBitmap(rotatedStar, null, starRatingRectF, null)
        }
        fillRating(canvas, rectFStroke1, rectFStroke2)
        canvas.drawRect(rectFBottomBackground, paintBackground)
    }

    private fun fillRating(canvas: Canvas, border1: RectF, border2: RectF) {
        val valueRectF = RectF()
        val diff = border2.left - border1.left
        valueRectF[border1.left + diff / 2, border1.top + diff / 4, border1.right - diff / 2] = border1.bottom - diff / 4
        if (value > 1) {
            canvas.drawArc(valueRectF, -180f, 30f, false, paintRedFillingStroke)
        } else {
            canvas.drawArc(valueRectF, -180f, (30 * value).toFloat(), false, paintRedFillingStroke)
            return
        }
        if (value > 2) {
            canvas.drawArc(valueRectF, -150f, 30f, false, paintRedFillingStroke)
        } else {
            canvas.drawArc(valueRectF, -150f, (30 * (value - 1)).toFloat(), false, paintRedFillingStroke)
            return
        }
        if (value > 3) {
            canvas.drawArc(valueRectF, -120f, 30f, false, paintYellowFillingStroke)
        } else {
            canvas.drawArc(valueRectF, -120f, (30 * (value - 2)).toFloat(), false, paintYellowFillingStroke)
            return
        }
        if (value > 4) {
            canvas.drawArc(valueRectF, -90f, 30f, false, paintYellowFillingStroke)
        } else {
            canvas.drawArc(valueRectF, -90f, (30 * (value - 3)).toFloat(), false, paintYellowFillingStroke)
            return
        }
        if (value > 4.5) {
            canvas.drawArc(valueRectF, -60f, 30f, false, paintGreenFillingStroke)
        } else {
            canvas.drawArc(valueRectF, -60f, (30 * (value - 3.5)).toFloat(), false, paintGreenFillingStroke)
            return
        }
        canvas.drawArc(valueRectF, -30f, (30 * 2 * (value - 4.5)).toFloat(), false, paintGreenFillingStroke)
    }

    override fun setAlpha(alpha: Int) {
        // Has no effect
    }

    override fun setColorFilter(cf: ColorFilter?) {
        // Has no effect
    }

    override fun getOpacity(): Int {
        // Not Implemented
        return PixelFormat.UNKNOWN
    }

    fun setValue(value: Double) {
        this.value = value
        invalidateSelf()
    }

    init {
        paintBackground.color = backgroundColor
        paintBackground.style = Paint.Style.FILL
        paintBackground.isAntiAlias = true
        paintBackground.isDither = true
        paintForeground = Paint()
        paintForeground.color = foregroundColor
        paintForeground.style = Paint.Style.FILL
        paintForeground.isAntiAlias = true
        paintForeground.isDither = true
        paintStroke = Paint()
        paintStroke.color = strokeColor
        paintStroke.style = Paint.Style.STROKE
        paintStroke.isAntiAlias = true
        paintStroke.strokeWidth = 1f
        paintStroke.isDither = true
        rectFBackground = RectF()
        rectFForeground = RectF()
        rectFStroke1 = RectF()
        rectFStroke2 = RectF()
        rectFBottomBackground = RectF()
        rectFSector = RectF()
        rectFAvatar = RectF()
        rectFSeparator = RectF()
        rectFStrokeColor = RectF()
        paintRed = Paint(Paint.ANTI_ALIAS_FLAG)
        paintRed.color = Color.parseColor("#CE6466")
        paintRed.style = Paint.Style.FILL
        paintRed.isAntiAlias = true
        paintRed.isDither = true
        paintYellow = Paint(Paint.ANTI_ALIAS_FLAG)
        paintYellow.color = Color.parseColor("#E5E756")
        paintYellow.style = Paint.Style.FILL
        paintYellow.isAntiAlias = true
        paintYellow.isDither = true
        paintGreen = Paint(Paint.ANTI_ALIAS_FLAG)
        paintGreen.color = Color.parseColor("#64CD64")
        paintGreen.style = Paint.Style.FILL
        paintGreen.isAntiAlias = true
        paintGreen.isDither = true
        paintRedStroke = Paint(Paint.ANTI_ALIAS_FLAG)
        paintRedStroke.color = Color.parseColor("#CE6466")
        paintRedStroke.style = Paint.Style.STROKE
        paintRedStroke.isAntiAlias = true
        paintRedStroke.strokeWidth = 2f
        paintRedStroke.isDither = true
        paintYellowStroke = Paint(Paint.ANTI_ALIAS_FLAG)
        paintYellowStroke.color = Color.parseColor("#E5E756")
        paintYellowStroke.style = Paint.Style.STROKE
        paintYellowStroke.isAntiAlias = true
        paintYellowStroke.strokeWidth = 2f
        paintYellowStroke.isDither = true
        paintGreenStroke = Paint(Paint.ANTI_ALIAS_FLAG)
        paintGreenStroke.color = Color.parseColor("#64CD64")
        paintGreenStroke.style = Paint.Style.STROKE
        paintGreenStroke.isAntiAlias = true
        paintGreenStroke.strokeWidth = 2f
        paintGreenStroke.isDither = true
        starBitmap = BitmapFactory.decodeResource(context.resources,
                R.drawable.ic_star)
        paintGreenFillingStroke = Paint(Paint.ANTI_ALIAS_FLAG)
        paintGreenFillingStroke.color = Color.parseColor("#64CD64")
        paintGreenFillingStroke.style = Paint.Style.STROKE
        paintGreenFillingStroke.isAntiAlias = true
        paintGreenFillingStroke.strokeWidth = 6f
        paintGreenFillingStroke.isDither = true
        paintYellowFillingStroke = Paint(Paint.ANTI_ALIAS_FLAG)
        paintYellowFillingStroke.color = Color.parseColor("#E5E756")
        paintYellowFillingStroke.style = Paint.Style.STROKE
        paintYellowFillingStroke.isAntiAlias = true
        paintYellowFillingStroke.strokeWidth = 6f
        paintYellowFillingStroke.isDither = true
        paintRedFillingStroke = Paint(Paint.ANTI_ALIAS_FLAG)
        paintRedFillingStroke.color = Color.parseColor("#CE6466")
        paintRedFillingStroke.style = Paint.Style.STROKE
        paintRedFillingStroke.isAntiAlias = true
        paintRedFillingStroke.strokeWidth = 6f
        paintRedFillingStroke.isDither = true
    }
}